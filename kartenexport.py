#!/usr/bin/env python

# Aufruf:
#
# virtualenv venv
# source venv/bin/activate
# pip install -r requirements.txt
# python -m offline_folium
# python kartenexport.py

print("Kartenexport")

import os
import json
import shapely

import osmnx as ox
import pandas as pd
import geopandas as gpd

ox.settings.log_console=False
ox.settings.use_cache=True

from offline_folium import offline
import folium
import folium.plugins as plugins

# Stadtpolygon
path_hh_geom = "hh-geometry.gpkg"
# Relativer Pfad zum hwf-geodaten-checkout
path_geodaten = "../geodaten"

loc_dammtor = (53.5602, 9.9910)

typen_props = { \
   "dclb":        {"digital":True,  "description":"DCLB",                "size":3},
   "dclp":        {"digital":True,  "description":"DCLP",                "size":2},
   "digisaeulen": {"digital":True,  "description":"Digitalsäule",        "size":2.5},
   "super-clb":   {"digital":True,  "description":"Riesen-Digitalboard", "size":4},
   "clb":         {"digital":False, "description":"CLB",                 "size":3},
   "clp":         {"digital":False, "description":"CLP",                 "size":2},
   "drehsaeulen": {"digital":False, "description":"Drehsäule",           "size":1.5},
   "fgu-mit-clp": {"digital":False, "description":"FGU mit CLP",         "size":2},
}

typen = list(typen_props.keys())

pointclouds = {}

print("Lade Punktdaten")

for typ in typen:
    filename = os.path.join(path_geodaten, "ohne_anmerkungen/groundtruth-%s_export.geojson" % typ)
    with open(filename) as file:
        data = json.load(file)
        pointclouds[typ] = data

def to_gtpoints(feats):
    for feat in feats:
        try:
            pt = shapely.geometry.Point(feat['geometry']['coordinates'])
        except ValueError as e:
            print("Error: %s" % feat)
        yield {'pos':pt, 'typ':typ}

gtpoints = []
for typ in typen:
    cloud = pointclouds[typ]
    gtpoints += to_gtpoints(cloud['features'])

df_gtpoints = pd.DataFrame(gtpoints)
gdf_gtpoints = gpd.GeoDataFrame(df_gtpoints)
gdf_gtpoints = gdf_gtpoints.set_geometry('pos', crs="EPSG:4326")
gt_sindex = gdf_gtpoints.sindex

df_gtpoints.drop_duplicates(subset=['pos', 'typ'], inplace=True)

print("%s Punkte geladen" % len(df_gtpoints))

print("Lade Stadtgeometrie")

stadtgebiet = gpd.read_file(path_hh_geom).geometry[0]
hambbox = l,t,r,b = stadtgebiet.bounds
waters = ox.features_from_polygon(stadtgebiet, tags={'natural': 'water'}).fillna('')
water_bodies = []

alles = [x for x in waters.geometry.items()]
for i in range(len(alles)):
    poly = alles[i][1]
    water_bodies += [poly]

water_union = shapely.ops.unary_union(water_bodies)
wbs2 = list(water_union.geoms)

hh_trocken = stadtgebiet.difference(water_union)

print("Gebe Karte aus")

m_schaubild = folium.Map(
    tiles='OpenStreetMap',
    location=loc_dammtor,
    prefer_canvas = True,
    control_scale=True,
    zoom_start=15,
    max_bounds=True,
    min_zoom = 11)

sim_geo = gpd.GeoSeries(stadtgebiet).simplify(tolerance=0.001).to_json()
geo_j = folium.GeoJson(data=sim_geo, style_function=lambda x: {"fillColor": "cornflowerblue"})
geo_j.add_to(m_schaubild)

for (i, entry) in enumerate( gtpoints ):
    coords = entry.get("pos").coords[0]
    descr  = typen_props.get(entry.get("typ"),{}).get("description")
    is_digital = typen_props.get(entry.get("typ"),{}).get("digital")
    size = typen_props.get(entry.get("typ"),{}).get("size")
    folium.Marker(location=(coords[1], coords[0]),
                  icon=plugins.BeautifyIcon(icon='',
                                            icon_size=(10*size, 10*size),
                                            border_width=2,
                                            background_color='yellow' if is_digital else 'white'),
                  popup=descr).add_to(m_schaubild)
    
m_schaubild.save("karte.html")
