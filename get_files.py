#!/usr/bin/env python3

# Auskommentieren für Verbindung ohne Proxy:
import socks
import socket
socks.set_default_proxy(socks.SOCKS5, "127.0.0.1", 9050)
socket.socket = socks.socksocket
import requests

import sys
import os

import osmnx as ox
import shapely

def download_werbeanlagen():
    """
    Werbeanlagen-Datensatz herunterladen
    """
    outfilename = "data/wa_gml_current.xml"

    if os.path.exists(outfilename):
        print("{} existiert schon.".format(outfilename))
        return

    with open(outfilename, "wb") as wa_file:
        print("Daten werden heruntergeladen (Geodienste Hamburg).")
        gml_req_url = "https://geodienste.hamburg.de/HH_WFS_Werbeanlagen?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&typename=de.hh.up:werbeanlagen"
        gml = requests.get(gml_req_url)
        wa_file.write(gml.content)

    wa_file.close()
    print("{} heruntergeladen.".format(outfilename))


def download_graph(mode):
    """
    Hamburg-Graph herunterladen (quadratischer Ausschnitt des Stadtgebiets mit etwas Umland)
    """
    outfilename = "data/hh-stadtgebiet-box-{}.graphml".format(mode)

    if os.path.exists(outfilename):
        print("{} existiert schon.".format(outfilename))
        return
        
    print("Daten werden heruntergeladen (Openstreetmap).")
    G = ox.graph_from_bbox(west = 9.7301155, south = 53.3951118, east = 10.3252805, north = 53.7394378, simplify=True, network_type=mode)
    ox.save_graphml(G, filepath=outfilename)
    
    print("{} heruntergeladen.".format(outfilename))


def download_hh_umrisse():
    """
    Polygone der Stadtkarte HH herunterladen
    """
    outfilename = "data/hh-geometry.gpkg"

    if os.path.exists(outfilename):
        print("{} existiert schon.".format(outfilename))
        return

    print("Daten werden heruntergeladen (Shapefile).")
    hh_gdf = ox.geocoder.geocode_to_gdf("Hamburg, Germany")
    # Gebaeude: {"building": True}
    mulpo = [x for x in hh_gdf.geometry.items()][0][1]
    # Versuch, die ausserhalb gelegene Insel Neuwerk abzuschneiden -> erfolgreich
    hh_proper = mulpo.geoms[1]
    hh_polygon = shapely.geometry.polygon.Polygon([(9.73,53.39), (10.326,53.39), (10.326,53.74), (9.73,53.74)])
    hh_stadtgebiet = hh_gdf.intersection(hh_polygon)
    hh_stadtgebiet.to_file(outfilename, driver="GPKG")
    
    print("{} heruntergeladen.".format(outfilename))



# mode='drive' ist das Straßennetz, weitere Optionen sind: 'bike', 'walk', ...
# https://geoffboeing.com/2016/11/osmnx-python-street-networks/

if __name__ == '__main__':
    download_werbeanlagen()
    download_graph(mode='drive')
    download_graph(mode='bike')
    # Das "walk"-Netz hat ca. ¼ Million Knoten und noch mehr Kanten.
    # download_graph(mode='walk')
    download_hh_umrisse()
