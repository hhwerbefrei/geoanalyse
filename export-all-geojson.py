#!/usr/bin/python

import re
import os
import sys

import argparse

import shlex, subprocess
from subprocess import PIPE

import datetime

def process(indir, outdir):
    today = datetime.datetime.now().replace(microsecond=0).isoformat()
    root, dirs, files = os.walk(indir, topdown=True).__next__()
    for filename in files:
        if re.match(r".*[.]geojson", filename, re.IGNORECASE):
            try:
#                outfilename = re.sub(".geojson", "", filename) + "_export_{}.geojson".format(today)
                outfilename = re.sub(".geojson", "", filename) + "_export.geojson"
                print("Processing {} -> {}".format(filename, outfilename))
                with open(os.path.join(root, filename), "r") as infile, open(os.path.join(outdir, outfilename), "w") as outfile:
                    proc = subprocess.Popen(shlex.split("jq -cM 'del(.features.[].properties.description)'"), stdin=infile, stdout=outfile, stderr=PIPE)
                    stdout, stderr = proc.communicate()
            except subprocess.CalledProcessError as e:
                print(f"Subprocess failed with return code {e.returncode}")
            
            

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('indir')
    parser.add_argument('-o', '--outdir', required=False, default=".")
    args = parser.parse_args()

    print("Processing files from {} into {}".format(args.indir, args.outdir))

    process(args.indir, args.outdir)
