# geoanalyse

Dies ist eine Einstiegshilfe zum Erstellen eigener Auswertungen der Daten aus dem [Geodatensatz Werbeanlagen Hamburg](https://metaver.de/trefferanzeige?cmd=doShowDocument&docuuid=0095A542-4D26-474D-BBC2-C95B87622E06)).

Der Datensatz steht unter der Lizenz [DL-DE-BY-2.0](https://www.govdata.de/dl-de/by-2-0). Der Quellenvermerk zum Zitieren ist: Freie und Hansestadt Hamburg, Behörde für Verkehr und Mobilitätswende, (BVM).

Es gibt eine Browseransicht im [Geoportal Hamburg](https://geoportal-hamburg.de/geo-online/?layerIds=12883,12884,16101,19969,19968,10158,18727&visibility=true,true,true,true,true,true,true&transparency=0,0,0,0,0,0,0&center=568987.077447925,5938843.763892601&zoomLevel=4#).

Anmerkung vom September 2022: Bedauerlicherweise hat die Nützlichkeit des Datensatzes abgenommen, seit die Anlagen in großem Stil durch digitale Anlagen ersetzt werden. Diese sind nicht zuverlässig gekennzeichnet, und die zuständige Behörde hat auf Anfrage [keine Lust](https://fragdenstaat.de/anfrage/daten-zu-digitalen-aussenwerbeanlagen-in-hamburg/), diese (z.B. um den Stromverbrauch beurteilen zu können) essentielle Information zur Verfügung zu stellen.

Auch hat die Behörde [keine Lust](https://fragdenstaat.de/anfrage/vollstaendige-daten-zu-aussenwerbeanlagen-auch-abgebauten-oder-ersetzten-in-hamburg-ab-dem-1-1-2000/), die Aufbaudaten umgerüsteter und ersetzter Anlagen korrekt zu kennzeichnen. Beides schmälert den Erkenntnisgewinn. Wir hoffen, dass sich das Problem durch systematische [Sichtung von Genehmigungen](https://community.codeforhamburg.org/search?q=werbe%20order%3Alatest_topic) und Crowdsourcing lösen lässt.

Schlimmer noch: beim Anlagentyp "Fahrgastunterstand" ist nicht klar, ob es sich immer nur um den reinen Haltestellen-Unterstand handelt, ob die eingebaute CLP-Anlage mitgemeint ist oder ob sich das von Fall zu Fall unterscheidet.

Um das alles zu klären und herauszufinden, wieviele Anlagen von welchem Typ es wirklich sind, bereiten wir nun einige neue Anfragen und eigene Erhebungen vor.

## loslegen

1. Herunterladen der Daten in weiterverarbeitbarem Format: mit dem Skript `get_files.py`

Das `osmnx`-Paket, das wir benutzen, kann auch selbst Daten herunterladen. Wir haben dieses Skript beigefügt um zu vermeiden, dass das immer wieder aufs Neue passiert (Ressourcenverschwendung!) und um Offline-Arbeit zu ermöglichen.

2. Starten des Jupyter-Notebooks im Docker-Container:

```
docker build files -f jupyter-dockerfile -t hwf
docker run -p 8888:8888 -v $PWD/data:/home/jovyan/data -v $PWD/notebooks:/home/jovyan/work hwf:latest
```

Es handelt sich um eine vorgefertigte Installation von Python, Jupyter und einigen wichtigen Paketen [jupyter-docker-stacks](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-minimal-notebook). Natürlich funktioniert das Ganze auch ohne Docker.

## unsere Auswertungen

* `notebooks/Anfang.ipynb`: weitgehend leer, als Vorlage für neue Arbeiten
* `notebooks/Auswerbung1.ipynb`: erste Exploration des Datensatzes
* `notebooks/AuswerbungCar.ipynb`: Routenbelastung und Zonengrafik (Pkw)
* `notebooks/AuswerbungBike.ipynb`: Routenbelastung und Zonengrafik (Fahrrad)

## troubleshooting

Wenn `get_files.py` fehlschlägt mit
```
  File "/usr/lib/python3.10/ctypes/__init__.py", line 392, in __getitem__
    func = self._FuncPtr((name_or_ordinal, self))
```
dann installiere python3-rtree nicht mit pip sondern so, dass die Lib `spatialindex` nicht fehlt.

## Erzeugen der Karte

Siehe `kartenexport.py`.


