let

  nixpkgs = fetchTarball "https://github.com/NixOS/nixpkgs/tarball/nixos-24.05";

  pkgs = import nixpkgs {
    config = { };
    overlays = [ ];
  };

in

pkgs.mkShell {

  #            packages = pkgs.python311.withPackages (ps: with ps; [notebook pandas numpy matplotlib seaborn osmnx plotly scikit-learn pip])
  #;
  packages = [
    pkgs.python311
    pkgs.python311Packages.pip
    pkgs.python311Packages.notebook
    pkgs.python311Packages.pandas
    pkgs.python311Packages.numpy
    pkgs.python311Packages.matplotlib
    pkgs.python311Packages.seaborn
    pkgs.python311Packages.osmnx
    pkgs.python311Packages.geopy
    pkgs.python311Packages.plotly
    pkgs.python311Packages.scikit-learn
  ];
  shellHook = ''
    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    python3 -m ipykernel install --user --name venv
  '';
}
