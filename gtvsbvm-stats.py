#!/usr/bin/env python3

import sys
import os

import osmnx as ox
import shapely

import os
import numpy as np
import osmnx as ox
import pandas as pd
import geopandas as gpd
from geopandas import GeoDataFrame
from shapely import wkt

from pyproj import CRS

# Darstellung mit Matplotlib
import matplotlib.pyplot as plt

# osmnx soll cache anlegen
ox.settings.log_console=False
ox.settings.use_cache=True

from pyproj import Proj, transform

import shapely
from shapely.geometry import Polygon
import geopy
from geopy import distance

import math
import json    
import re

# def find_doublets(wa):
import sklearn

hamburg = ox.load_graphml("data/hh-stadtgebiet-box-drive.graphml")

umapdatafile = "data/gtvsbvm.umap"

if len(sys.argv)>1:
    umapdatafile = sys.argv[1]

print("Loading umap data ...")

with open(umapdatafile) as file:
  umap = json.load(file)


if umap.get('layers')[0].get('_umap_options').get('name') != 'Ground Truth':
    print("layer 0 wrong name, %s" % umap.get('layers')[0].get('_umap_options').get('name'))
    sys.exit(-1)

if umap.get('layers')[1].get('_umap_options').get('name') != 'coverage':
    print("layer 1 wrong name, %s" % umap.get('layers')[0].get('_umap_options').get('name'))
    sys.exit(-1)

if len(umap.get('layers')) > 2:
    print("unexpected number of layers")
    sys.exit(-1)

ground = umap.get('layers')[0]
coverage = umap.get('layers')[1]

cov = []
for feat in coverage['features']:
    geom = feat['geometry']
    if geom['type'] == 'Polygon':
        # print(geom['coordinates'][0])
        poly = shapely.geometry.Polygon(geom['coordinates'][0])
        cov += [poly]

cov_union = shapely.ops.unary_union(cov)

# Albers Equal Area
parea = Proj("+proj=aea +lat_1=54.0 +lat_2=9.0 +lat_0=56.0 +lon_0=11.0")

sum = 0

for geom in cov_union.geoms:
    lon, lat = zip(*geom.exterior.coords)
    x, y = parea(lon,lat)
    
    cop = {"type": "Polygon", "coordinates": [zip(x, y)]}
    from shapely.geometry import shape
    # print (shape(cop).area)
    sum += shape(cop).area

# was sind das, Quadratmeter?
# print("Quadratkilometer: %f" % (math.sqrt(sum) / 1000.0))


print("Loading HH geometry data ...")
stadtgebiet = gpd.read_file("data/hh-geometry.gpkg").geometry[0]
hambbox = l,t,r,b = stadtgebiet.bounds
waters = ox.geometries_from_polygon(stadtgebiet, tags={'natural': 'water'}).fillna('')

water_bodies = []

alles = [x for x in waters.geometry.items()]
for i in range(len(alles)):
    poly = alles[i][1]    
    water_bodies += [poly]

water_union = shapely.ops.unary_union(water_bodies)
wbs2 = list(water_union.geoms)

hh_trocken = shapely.difference(stadtgebiet, water_union)

def plot_multipoly(multipoly, color):
  for poly in multipoly.geoms:
    plt.fill(*poly.exterior.xy, color=color)
    
    if poly.interiors:
        for inner in poly.interiors:
            plt.fill(*inner.xy, color="w")   

fig, ax = plt.subplots(1, 1, figsize=(16, 16))
plot_multipoly(hh_trocken, "lightgreen")
plot_multipoly(hh_trocken.intersection(cov_union), "brown")


covperc = hh_trocken.intersection(cov_union).area / hh_trocken.area * 100

print("Coverage: %f %%" % covperc)

inters = hh_trocken.intersection(cov_union)


print("Collecting ...")

gtpoints = []
for feat in ground['features']:
    try:
      pt =  shapely.geometry.Point(feat['geometry']['coordinates'])
    except ValueError as e:
      print("Error: %s" % feat)
      continue
    # print(feat['properties'].keys())
    # print(feat['properties']['name'])
    # print(feat['properties']['description'])
    # print(feat['properties']['_umap_options'].get('color'))
    
    name = feat['properties'].get('name', '')
    desc = feat['properties'].get('description', '')
    color = feat['properties'].get('_umap_options', {}).get('color', '')

    
    gtpoints += [{'pos':pt, 'name':name, 'color':color, 'desc':desc}]
    
df_gtpoints = pd.DataFrame(gtpoints)
gdf_gtpoints = gpd.GeoDataFrame(df_gtpoints)
gdf_gtpoints = gdf_gtpoints.set_geometry('pos', crs="EPSG:4326")
gt_sindex = gdf_gtpoints.sindex


stuecke = ox.utils_geo._quadrat_cut_geometry(inters, quadrat_width=0.015, min_num=3)



print("Filtering ...")


gt_points_inside = pd.DataFrame()

matchess = []

for part in stuecke.geoms:

    part = part.buffer(1e-14).buffer(0)
    
    possible_matches_index = list(gt_sindex.intersection(part.bounds))
    possible_matches = gdf_gtpoints.iloc[possible_matches_index]
    precise_matches = possible_matches[possible_matches.intersects(part)]
    matchess += [precise_matches]

gt_points_inside = pd.concat(matchess)

gt_points_inside = gt_points_inside.drop_duplicates(subset=['pos'])
gt_points_outside = gdf_gtpoints[~gdf_gtpoints.isin(gt_points_inside)]


filtered = gt_points_inside
# filtered = df_gtpoints.loc[df_gtpoints['pos'].apply(lambda pt : inters.contains(pt))]

(total_inside, digi_inside) = len(filtered[filtered['color'] == 'Fuchsia']) + len(filtered[filtered['color'] == 'Red']), len(filtered[filtered['color'] == 'Red'])

digi_all = len(df_gtpoints[df_gtpoints['color']=='Red'])
anal_all = len(df_gtpoints[df_gtpoints['color']=='Fuchsia'])
(digi_all, digi_all + anal_all)

print("Gesichert: %d, davon %d digital" % (total_inside, digi_inside))
print("Total: %d" % (digi_all + anal_all))

